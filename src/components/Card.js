import React from 'react';
import {View, StyleSheet} from 'react-native'

const Card = (props) => {
    return <View onLayout={props.onLayout} style={[styles.container, props.style]}>
        {props.children}
    </View>
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 0,
        borderRadius: 3,
        borderColor: '#ddd',
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: '#fff',
        overflow: "hidden"
    }
});

export default Card;