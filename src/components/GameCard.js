import React from 'react';
import {View,Text,StyleSheet,Image} from 'react-native';

const GameCard = (props) => {
    let iconProfile = props.game.opponent_avatar ? {uri: props.game.opponent_avatar} : require('../assets/icons/user.png')
    return <View onLayout={props.onLayout} style={[styles.container, props.style]}>
        <View>
            <View style={styles.wrapper}>
                <Image style={styles.profilepic}
                       source={require('../assets/images/halep.jpg')}
                />
                <View style={{alignSelf: 'center'}}>
                    <Text>VS</Text>
                </View>
                <Image style={styles.profilepic}
                       source={iconProfile}/>
            </View>
            <View style={styles.wrapper}>
                <Text style={styles.textPlayerName}>Simona Halep</Text>
                <Text style={styles.textPlayerName}>{props.game.opponent_name}</Text>
            </View>
            <View style={styles.wrapper}>
                <View style={styles.cardScore}>
                    <View style={styles.cardScoreTitle}>
                        <Text style={styles.textScoreTitle}>Score</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={styles.textPlayerName}>Simona Halep</Text>
                            <Text style={styles.textPlayerName}>{props.game.opponent_name}</Text>
                        </View>
                        {props.game.score.set1 ?
                        <View style={styles.scoreSet}>
                            <Text>{props.game.score.set1[0]}</Text>
                            <Text>{props.game.score.set1[1]}</Text>
                        </View> : null}
                        {props.game.score.set2 ?
                            <View style={styles.scoreSet}>
                            <Text>{props.game.score.set2[0]}</Text>
                            <Text>{props.game.score.set2[1]}</Text>
                        </View> : null}
                        {props.game.score.set3 ?
                            <View style={styles.scoreSet}>
                            <Text>{props.game.score.set3[0]}</Text>
                            <Text>{props.game.score.set3[1]}</Text>
                        </View> : null}
                        {props.game.score.set4 ?
                            <View style={styles.scoreSet}>
                            <Text>{props.game.score.set4[0]}</Text>
                            <Text>{props.game.score.set4[1]}</Text>
                        </View> : null}
                    </View>
                </View>
            </View>
            <View style={styles.footerWrapper}>
                <Text style={[styles.textLocation,{color: '#000',}]}>Location: </Text>
                <Text style={styles.textLocation}>{props.game.address}</Text>
            </View>
        </View>
    </View>
};

const styles = StyleSheet.create({
    textLocation: {
        fontSize: 16,
    },
    footerWrapper: {
        paddingRight: 24,
        paddingLeft: 24,
        paddingBottom: 12,
        flexDirection: 'row',
    },
    scoreSet: {
        paddingLeft: 24,
        flexDirection: 'column'
    },
    textScoreTitle: {
        color: '#000',
        fontSize: 16,
    },
    cardScoreTitle: {
        backgroundColor: '#e9e9e9',
        justifyContent: 'center',
        alignItems: 'center'
    },
    cardScore: {
        // borderWidth: 1,
        // borderColor: '#000',
        flex: 1,
        marginBottom: 12
    },
    textPlayerName: {
        fontSize: 14,
        color: '#000'
    },
    wrapper: {
        paddingTop: 12,
        paddingRight: 24,
        paddingLeft: 24,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    profilepic: {
        width: 80,
        height: 80,
        borderRadius: 40
    },
    profilepicWrap: {
        paddingTop: 12,
        paddingRight: 24,
        paddingLeft: 24

    },
    container: {
        borderWidth: 0,
        borderRadius: 3,
        borderColor: '#ddd',
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        backgroundColor: '#fff',
        overflow: "hidden"
    }
});

export default GameCard;