import React,{Component} from 'react';
import {View, Text, TextInput, Button, StyleSheet, AsyncStorage,Alert} from 'react-native';
import {observer} from 'mobx-react';
import {observable} from 'mobx';


@observer class LoginPage extends Component {

    @observable data = {
        email: '',
        password: ''
    };

    render()
    {
        return <View style={{flex: 1}}>
            <View style={styles.container}>
                <Text style={styles.titleContainer}>Login</Text>
                <TextInput
                    value={this.data.email}
                    onChangeText={(email) => this.data.email = email}
                    placeholder={'Email'}
                    keyboardType={'email-address'}
                    style={styles.input}
                    onSubmitEditing={() => { this.password.focus(); }}
                    returnKeyType={ "next" }
                    selectTextOnFocus={false}
                />
                <TextInput
                    value={this.data.password}
                    ref={(input) => { this.password = input; }}
                    onChangeText={(password) => this.data.password = password}
                    placeholder={'Password'}
                    secureTextEntry = {true}
                    style={styles.input}
                    selectTextOnFocus={false}
                />
                <View style={{flexDirection: 'row'}}>
                    <View style={{paddingRight: 6,paddingLeft: 6}}>
                        <Button
                            title={'Login'}
                            color="#841584"
                            onPress={this.onLogin.bind(this)}
                        />
                    </View>
                    <View style={{paddingRight: 6,paddingLeft: 6}}>
                        <Button
                            title={'Register'}
                            color="#841584"
                            onPress={this.onRegister.bind(this)}
                        />
                    </View>
                </View>
            </View>
        </View>
    }

    onRegister()
    {
        this.props.navigation.navigate('RegisterPage');
    }

    onLogin() {
        let self = this;

        if(this.data.email.length > 0 && this.data.password.length > 0) {

            fetch('https://racket-mate-agfsbfyiap.now.sh/api/v1/users/login', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: this.data.email,
                    password: this.data.password,
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    if(responseJson.token)
                    {
                        AsyncStorage.setItem('QToken', responseJson.token);
                        self.props.navigation.navigate('AccountList',{token: responseJson.token});
                        self.data.email = '';
                        self.data.password = '';
                    }
                    else if(responseJson.message)
                    {

                        Alert.alert(
                            'Error',
                            responseJson.message,
                        );
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        else
        {
            Alert.alert(
                'Error',
                'Complete all fields',
            );
        }

    }
}

const styles = StyleSheet.create({
    titleContainer: {
        fontSize: 24,
        marginBottom: 12
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 25
    },
});

export default LoginPage;