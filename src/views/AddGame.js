import React, {Component} from 'react';
import {Alert, Button, StyleSheet, Text, TextInput, View} from 'react-native';
import {observable} from "mobx";
import {observer} from 'mobx-react';

@observer class AddGame extends Component {

    @observable data = {
        opponentName: '',
        score: '',
        address: '',
        lat: '',
        long: ''
    };
    render()
    {
        return <View style={{flex: 1}}>
            <View style={styles.container}>
                <Text style={styles.titleContainer}>Add Game</Text>
                <TextInput
                    value={this.data.opponentName}
                    onChangeText={(opponentName) => this.data.opponentName = opponentName}
                    placeholder={'Opponent Name'}
                    style={styles.input}
                    onSubmitEditing={() => { this.score.focus(); }}
                    returnKeyType={ "next" }
                    selectTextOnFocus={false}
                />
                <TextInput
                    ref={(input) => { this.score = input; }}
                    value={this.data.score}
                    onChangeText={(score) => this.data.score = score}
                    placeholder={'Score'}
                    style={styles.input}
                    onSubmitEditing={() => { this.address.focus(); }}
                    returnKeyType={ "next" }
                    selectTextOnFocus={false}
                />
                <TextInput
                    value={this.data.address}
                    ref={(input) => { this.address = input; }}
                    onChangeText={(address) => this.data.address = address}
                    placeholder={'Address'}
                    style={styles.input}
                    selectTextOnFocus={false}
                    onSubmitEditing={() => { this.lat.focus(); }}
                />
                <TextInput
                    value={this.data.lat}
                    ref={(input) => { this.lat = input; }}
                    onChangeText={(lat) => this.data.lat = lat}
                    placeholder={'Lat'}
                    style={styles.input}
                    selectTextOnFocus={false}
                    onSubmitEditing={() => { this.long.focus(); }}
                />
                <TextInput
                    value={this.data.long}
                    ref={(input) => { this.long = input; }}
                    onChangeText={(long) => this.data.long = long}
                    placeholder={'Long'}
                    style={styles.input}
                    selectTextOnFocus={false}
                />

                <View style={{flexDirection: 'row'}}>
                    <View style={{paddingRight: 6,paddingLeft: 6}}>
                        <Button
                            title={'Add Game'}
                            color="#841584"
                            onPress={this.addGame.bind(this)}
                        />
                    </View>
                </View>
            </View>
        </View>
    }

    addGame() {
        if(this.data.opponentName.length > 0
            && this.data.score.length > 0
            && this.data.address.length > 0
            && this.data.lat.length > 0
            && this.data.long.length > 0) {

            fetch('https://racket-mate-agfsbfyiap.now.sh/api/v1/game', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': this.props.navigation.state.params.token
                },
                body: JSON.stringify({
                    opponentName: this.data.fullname,
                    score: this.data.score,
                    address: this.data.password,
                    lat: this.data.lat,
                    long: this.data.long,
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    console.log('response',responseJson);
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        else
        {
            Alert.alert(
                'Error',
                'Complete all fields',
            );
        }

    }
}

const styles = StyleSheet.create({
    titleContainer: {
        fontSize: 24,
        marginBottom: 12
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 25
    },
});

export default AddGame;