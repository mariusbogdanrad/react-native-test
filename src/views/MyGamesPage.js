import React,{Component} from 'react';
import {View, Text, ScrollView, StyleSheet,ActivityIndicator} from 'react-native';
import {observer} from 'mobx-react';
import {observable} from 'mobx';
import GameCard from "../components/GameCard";
import Card from "../components/Card";

@observer class MyGamesPage extends Component {

    @observable data = {
        games: [],
        loading: true,
    };

    componentDidMount()
    {

        this.getGames();
    }

    getGames()
    {
        let self = this;

        fetch('https://racket-mate-agfsbfyiap.now.sh/api/v1/game', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': this.props.navigation.state.params.token
            },
        }).then((response) => response.json())
            .then((responseJson) => {
                self.data.loading = false;
                self.data.games = responseJson.rows;
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render()
    {
        if(this.data.loading)
        {
            return <View style={{flex:1}}>
                <ActivityIndicator
                    color={"#d82a21"}
                    size={'large'}
                    style={{flex: 1}}/>
            </View>
        }

        return <ScrollView>
            <View style={{paddingTop: 12, paddingBottom: 12}}>
                <Text style={styles.textTitle}>My Games</Text>
            </View>
            <View style={{paddingBottom: 24}}>
                {this.data.games.length > 0 ? this._renderGames() :
                <Card>
                    <View style={{paddingTop: 12,paddingBottom:12}}>
                        <Text style={{fontSize: 18,color:"#000",textAlign: 'center'}}>You don't have games</Text>
                    </View>
                </Card>}
            </View>
        </ScrollView>
    }

    _renderGames()
    {
        return this.data.games.map(game =>
            <GameCard key={game.id} game={game}/>
        );
    }
}

const styles = StyleSheet.create({
    textTitle: {
        fontSize: 24,
        color: '#000',
        textAlign: 'center'
    },
});

export default MyGamesPage;