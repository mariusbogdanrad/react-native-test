import React,{Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, Alert,AsyncStorage} from 'react-native';
import {inject,observer} from 'mobx-react';

@observer

class AccountList extends Component {


    logoutConfirm() {
        return (
            Alert.alert(
                'Are you sure?',
                '',
                [
                    {text: 'No', onPress: () => false, style: 'cancel'},
                    {text: 'Yes', onPress: () => this.logout()},
                ],
                { cancelable: false }
            )
        )
    }

    logout() {
        AsyncStorage.removeItem('QToken');
        this.props.navigation.navigate('LoginPage');
    }


    _renderAccountList()
    {
        return <View style={styles.AccountListItemContainer}>
            <TouchableOpacity delayPressIn={50} onPress={() => {this.props.navigation.navigate('MyGamesPage',{token: this.props.navigation.state.params.token})}}>
                <View style={styles.AccountListItem}>
                    <Image style={styles.AccountListItemImage} source={require('../assets/icons/my-games.png')}/>
                    <View style={styles.AccountListItemTextContainer}>
                        <Text style={styles.AccountListItemTitleText}>My Games</Text>
                    </View>
                </View>
            </TouchableOpacity>
            <TouchableOpacity delayPressIn={50} onPress={() => {this.props.navigation.navigate('AddGame',{token: this.props.navigation.state.params.token})}}>
                <View style={styles.AccountListItem}>
                    <Image style={styles.AccountListItemImage} source={require('../assets/icons/add-match.png')}/>
                    <View style={styles.AccountListItemTextContainer}>
                        <Text style={styles.AccountListItemTitleText}>Add Game</Text>
                    </View>
                </View>
            </TouchableOpacity>
            <TouchableOpacity delayPressIn={50} onPress={() => {this.logoutConfirm()}}>
                <View style={styles.AccountListItem}>
                    <Image style={styles.AccountListItemImage} source={require('../assets/icons/logout.png')}/>
                    <View style={styles.AccountListItemTextContainer}>
                        <Text style={styles.AccountListItemTitleText}>Logout</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    }

    _renderHeader()
    {
        return (
            <View style={styles.headerAccountListItem}>
                <View>
                    <Text style={styles.headerAccountListItemTextTitle}>Welcome</Text>
                </View>
            </View>
        )
    }

    render()
    {
        return(
            <ScrollView>
                {this._renderHeader()}
                {this._renderAccountList()}
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    AccountListItemContainer: {
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 26
    },
    headerAccountListItem: {
        flex: 1,
        paddingTop: 28,
        justifyContent: "center",
        alignItems: 'center'
    },
    headerAccountListItemTextTitle: {
        fontSize: 28,
        color: '#000',
        alignSelf:'center'
    },
    AccountListItemTextContainer: {
        flex:1,
        flexDirection: 'column',
        justifyContent: "center",
        paddingLeft: 8,
    },
    AccountListItemTitleText: {
        fontSize: 18,
        color: '#000',
        marginBottom: 4,
    },
    AccountListItemImage: {
        width: 48,
        height: 48,
    },
    AccountListItem: {
        elevation: 1,
        marginLeft: 8,
        marginRight: 5,
        marginTop: 10,
        flexDirection: 'row',
    }
});
export default AccountList;