import React from 'react';
import {createStackNavigator } from 'react-navigation';
import LoginPage from "./views/LoginPage";
import RegisterPage from "./views/RegisterPage";
import MyGamesPage from "./views/MyGamesPage";
import AccountList from "./views/AccountList";
import AddGame from "./views/AddGame";
import {Image, View,TouchableOpacity,Text} from "react-native";

const MainNavigator = createStackNavigator({
    LoginPage: {
        screen: LoginPage,
    },
    RegisterPage: {
        screen: RegisterPage,
    },
    AccountList: {
        screen: AccountList,
    },
    MyGamesPage: {
        screen: MyGamesPage,
    },
    AddGame: {
        screen: AddGame,
    },
}, {
    initialRouteName: 'LoginPage',
    backBehavior: 'LoginPage',
    navigationOptions: {
        header: ({navigation}) => {
            return <View style={{
                flexDirection: 'row',
                justifyContent: "center",
                backgroundColor: '#ffffff',
                shadowOffset:{  width: 0,  height: 1},
                shadowColor: '#000000',
                shadowOpacity: 1.0,
                elevation: 2,
                zIndex: 2}}>
                {navigation.state.routes.slice(-1)[0].routeName !== 'LoginPage' ?
                    <TouchableOpacity  style={{ position: "absolute", bottom: 9, left: 12, width: 32}} onPress={()=>  navigation.goBack(null)}>
                        <Image style={{height: 32, width: 32}} source={require('./assets/icons/back_arrow.png')}/>
                    </TouchableOpacity>
                    : null}
                <Image style={{height: 48, width: 130,resizeMode:'contain'}} source={require('./assets/logo.png')}/>
            </View>
        },
    },
    transitionConfig: () => {return null},
});

export default MainNavigator