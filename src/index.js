import React from 'react';
import { View, StatusBar,AsyncStorage } from 'react-native'
import MainNavigator from './routes';
import {Provider,observer} from 'mobx-react';
import stores from './stores';
console.disableYellowBox = true;

@observer class App extends React.Component {

    render() {
        return  <Provider {...stores}>
            <View style={{flex: 1}}>
                <StatusBar barStyle="light-content" />
                <MainNavigator/>
            </View>
        </Provider>
    }

};

export default App;