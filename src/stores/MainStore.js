import {observable, action, computed} from 'mobx';
import {AsyncStorage} from 'react-native';

class MainStore {

    @observable user;

    @action setUser = (newData) => {
        this.user = newData;
        this.logged = this.user ? true : false;
    };

    @action logout = () => {
        AsyncStorage.removeItem('QToken');
    };


}

export default MainStore;